package it.rcs.fuse.core.route;

import java.util.Properties;

import javax.annotation.PostConstruct;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.properties.DefaultPropertiesParser;
import org.apache.camel.component.properties.PropertiesComponent;
import org.apache.camel.impl.CompositeRegistry;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.impl.SimpleRegistry;
import org.apache.deltaspike.core.api.config.ConfigResolver;

public abstract class BaseRouteBuilder extends RouteBuilder{
	private static final String APACHE_DELTASPIKE_PATH = "/META-INF/apache-deltaspike.properties";
	
	public static final String PROPERTY_SISTEMA = "SISTEMA";
	
	public static final String START = "START";
	public static final String STOP = "STOP";
	
	public static final String APPLICATION_JSON = "application/json";
	
	public static final String policy1s = "policyTimeout1s";
	public static final String policy3s = "policyTimeout3s";
	public static final String policy5s = "policyTimeout5s";
	public static final String policy6s = "policyTimeout6s";
	public static final String policy7s = "policyTimeout7s";
	public static final String policy8s = "policyTimeout8s";
	public static final String policy9s = "policyTimeout9s";
	public static final String policy10s = "policyTimeout10s";
	public static final String policy15s = "policyTimeout15s";
	public static final String policy30s = "policyTimeout30s";
	public static final String policy45s = "policyTimeout45s";
	public static final String policy90s = "policyTimeout90s";
	public static final String policy150s = "policyTimeout150s";
	
	public enum SISTEMA{
		SOA, RCS;
	}
	
	@PostConstruct
	void manual() {
		setupDeltaspike();
	}
	
	@Override
	public void configure() throws Exception {
		if(getContext()!=null) {
			setupDeltaspike();
			components();
			dataSources();
			resourceAdapters();
			policyEndpointConfigurer();
			threadPoolDefinition();
		}
	}
	
	/* Unimplemented methods */
	protected abstract void components();
	
	protected abstract void dataSources();
	
	protected abstract void resourceAdapters() throws Exception;
	
	protected abstract void policyEndpointConfigurer();
	
	protected abstract void threadPoolDefinition();
	
	/* Implemented methods */ 
	static class DeltaSpikeParser extends DefaultPropertiesParser{
		@Override
		public String parseProperty(String key, String value, Properties properties) {
			if(value == null) {
				return ConfigResolver.getPropertyValue(key);
			} else {
				return value;
			}
		}
	}
	
	protected void setupDeltaspike() {
		if (getContext() != null) {
			PropertiesComponent component = getContext().getComponent("properties", PropertiesComponent.class);
			if(component == null) {
				component = new PropertiesComponent();
				component.setInitialProperties(org.apache.deltaspike.core.api.config.PropertyLoader.getProperties(APACHE_DELTASPIKE_PATH));
				component.setPropertiesParser(new DeltaSpikeParser());
			} else {
				component.setInitialProperties(org.apache.deltaspike.core.api.config.PropertyLoader.getProperties(APACHE_DELTASPIKE_PATH));
				component.setPropertiesParser(new DeltaSpikeParser());
			}
			
		}
	}
	
	protected void putOnRegistry(String key, Object value) {
		SimpleRegistry simpleRegistry = new SimpleRegistry();
		simpleRegistry.put(key, value);
		CompositeRegistry compositeRegistry = new CompositeRegistry();
		compositeRegistry.addRegistry(simpleRegistry);
		compositeRegistry.addRegistry(getContext().getRegistry());
		((DefaultCamelContext) getContext()).setRegistry(compositeRegistry);
	}
}

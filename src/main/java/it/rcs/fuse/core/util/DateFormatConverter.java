package it.rcs.fuse.core.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

public class DateFormatConverter {
	
	public static final String SDF_MIN_YYYYMMDD = "yyyy-MM-dd";
	
	private DateFormatConverter(){}

	public static XMLGregorianCalendar dateToXMLGregorianCalendar(Date date) throws DatatypeConfigurationException{
		return DatatypeFactory.newInstance().newXMLGregorianCalendar(new SimpleDateFormat(SDF_MIN_YYYYMMDD).format(date));
	}
	
	public static String xmlGregorianCalendarToString(XMLGregorianCalendar xc, String format){
		Date date = xmlGregorianCalendarToDate(xc);
        return new SimpleDateFormat(format).format(date);
	}
	
	public static Date xmlGregorianCalendarToDate(XMLGregorianCalendar xc){
		GregorianCalendar gCalendar = xc.toGregorianCalendar();
        return gCalendar.getTime();
	}
	
	public static String xmlGregorianCalendarToString(XMLGregorianCalendar xc){
        return DateFormatConverter.xmlGregorianCalendarToString(xc,SDF_MIN_YYYYMMDD);
	}
	
	public static XMLGregorianCalendar stringToXMLGregorianCalendar(String date) throws DatatypeConfigurationException, ParseException{
		return DatatypeFactory.newInstance().newXMLGregorianCalendar(new SimpleDateFormat(SDF_MIN_YYYYMMDD).format(new SimpleDateFormat(SDF_MIN_YYYYMMDD).parse(date)));
	}
		
	public static XMLGregorianCalendar dateToXMLGregorianCalendar(Date date, SimpleDateFormat sdf) throws DatatypeConfigurationException{
		return DatatypeFactory.newInstance().newXMLGregorianCalendar(sdf.format(date));
	}

	public static XMLGregorianCalendar stringToXMLGregorianCalendar(String date, SimpleDateFormat sdf) throws DatatypeConfigurationException, ParseException{
		return DatatypeFactory.newInstance().newXMLGregorianCalendar(new SimpleDateFormat(SDF_MIN_YYYYMMDD).format(sdf.parse(date)));
	}
	
	public static String fromStringToString(String strDate,String format) throws ParseException {
		return new SimpleDateFormat(SDF_MIN_YYYYMMDD).format(new SimpleDateFormat(format).parse(strDate));
	}
	
	public static String fromDateToString(Date date,String format) throws ParseException {
		return new SimpleDateFormat(format).format(date);
	}
	
	public static String fromDateToString(Date date, SimpleDateFormat sdf) {
		return sdf.format(date);
	}
	
}

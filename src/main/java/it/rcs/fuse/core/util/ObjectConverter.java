package it.rcs.fuse.core.util;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class ObjectConverter {
	private ObjectConverter(){
		
	}
	public static String convertObjectToJson(Object bean) throws JsonProcessingException{
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		mapper.configure(SerializationFeature.WRITE_EMPTY_JSON_ARRAYS, false);
		return mapper.writeValueAsString(bean);
	}
	public static <T> T convertJsonToObject(String jsonInString,Class<T> type) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		return type.cast(mapper.readValue(jsonInString, type));
	}
	
}

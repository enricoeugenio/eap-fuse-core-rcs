package it.rcs.fuse.core.exception;

public class ResourceNotFoundException extends Exception{

	private String sistema;
	
	public ResourceNotFoundException(String sistema) {
		this.sistema=sistema;
	}

	public String getSistema(){
		return sistema;
	}
	

	private static final long serialVersionUID = 1L;
	
}

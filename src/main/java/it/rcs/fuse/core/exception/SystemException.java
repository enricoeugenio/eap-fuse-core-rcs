package it.rcs.fuse.core.exception;

import it.rcs.fuse.core.bean.FaultList;

public class SystemException extends RuntimeException{
	private static final long serialVersionUID = 1L;
	private FaultList faultList;
	
	public SystemException(){
		
	}
	
	public SystemException(FaultList faultList){
		this.faultList = faultList;
	}
	
	public FaultList getFaultList() {
		return faultList;
	}
	public void setFaultList(FaultList faultList) {
		this.faultList = faultList;
	}
}

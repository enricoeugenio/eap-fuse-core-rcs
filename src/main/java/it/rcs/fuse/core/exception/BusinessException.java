package it.rcs.fuse.core.exception;

import it.rcs.fuse.core.bean.FaultList;

public class BusinessException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	private FaultList faultList;

	public BusinessException() {
		super();
	}

	public BusinessException(FaultList faultList) {
		this.faultList = faultList;
	}

	public FaultList getFaultList() {
		return faultList;
	}

	public void setFaultList(FaultList faultList) {
		this.faultList = faultList;
	}

}

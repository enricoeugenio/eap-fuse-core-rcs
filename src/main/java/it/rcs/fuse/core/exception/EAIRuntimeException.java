package it.rcs.fuse.core.exception;

public class EAIRuntimeException extends RuntimeException {

	public EAIRuntimeException(Exception e) {
		super(e);
	}

	private static final long serialVersionUID = 1L;

}

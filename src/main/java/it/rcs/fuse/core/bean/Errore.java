package it.rcs.fuse.core.bean;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import it.rcs.fuse.core.util.ObjectConverter;

@JsonInclude(Include.NON_EMPTY)
public class Errore {

	public static final String CODICE_TIMEOUT = "1";
	public static final String CODICE_ERRORE_GENERICO = "2";
	public static final String CODICE_VALID = "2";
	public static final String CODICE_DATI_NON_TROVATI = "7272";
	public static final String CODICE_PREDICATE_EXCPETION = "4";

	
	private String codice;
	private String descrizione;
	private String sistema;

	public String getCodice() {
		return codice;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public String getSistema() {
		return sistema;
	}

	public void setSistema(String sistema) {
		this.sistema = sistema;
	}

	public String toString(){
		try {
			return ObjectConverter.convertObjectToJson(this);
		} catch (Exception ex) {
			return super.toString();
		}
	}

}

package it.rcs.fuse.core.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import it.rcs.fuse.core.util.ObjectConverter;

@JsonInclude(Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FaultList implements Serializable {

	private static final long serialVersionUID = 1L;
	private List<Errore> faultList;

	public FaultList(List<Errore> aList) {
		faultList = aList;
	}

	public FaultList() {
		faultList = new ArrayList<Errore>();
	}

	public FaultList(Errore errore) {
		faultList = new ArrayList<Errore>();
		faultList.add(errore);
	}

	public List<Errore> getFaultList() {
		return faultList;
	}

	public void setFaultList(List<Errore> faultList) {
		this.faultList = faultList;
	}

	public String toString() {
		try {
			return ObjectConverter.convertObjectToJson(this);
		} catch (Exception ex) {
			return super.toString();
		}
	}
}

package it.rcs.fuse.core.dataformat;

import java.io.StringWriter;

import org.apache.camel.component.jackson.JacksonDataFormat;
import org.apache.camel.spi.DataFormat;

import com.fasterxml.jackson.databind.DeserializationFeature;

public class JSONDataFormat extends JacksonDataFormat implements DataFormat{

	public JSONDataFormat() {
		super();
		this.setPrettyPrint(true);
		StringWriter enableFeatures=new StringWriter();
		enableFeatures.append(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY.name());
		enableFeatures.append(",");
		enableFeatures.append(DeserializationFeature.USE_JAVA_ARRAY_FOR_JSON_ARRAY.name());
		
		StringWriter disableFeatures=new StringWriter();
		disableFeatures.append(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES.name());
		disableFeatures.append(",");
		disableFeatures.append(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES.name());
		disableFeatures.append(",");
		disableFeatures.append(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES.name());
		
		this.setEnableFeatures(enableFeatures.toString());
		this.setDisableFeatures(disableFeatures.toString());
	}

	public JSONDataFormat(Class<?> unmarshalType) {
		this();
		this.setUnmarshalType(unmarshalType);
	}

	public JSONDataFormat(String unmarshalType) throws Exception {
		this();
		if (unmarshalType != null) {
			this.setUnmarshalType(Class.forName(unmarshalType));
		}
	}

	public void setUnmarshalType(String unmarshalType) throws Exception {
		super.setUnmarshalType(Class.forName(unmarshalType));
	}
	
}

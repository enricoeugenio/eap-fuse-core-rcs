package it.rcs.fuse.core.converter;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.concurrent.RejectedExecutionException;

import org.apache.camel.Converter;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangeTimedOutException;
import org.apache.camel.processor.validation.PredicateValidationException;

import it.rcs.fuse.core.bean.Errore;
import it.rcs.fuse.core.bean.FaultList;
import it.rcs.fuse.core.exception.ResourceNotFoundException;
import it.rcs.fuse.core.route.BaseRouteBuilder;
import it.rcs.fuse.core.route.BaseRouteBuilder.SISTEMA;

@Converter
public class ErroreConverter {

	@Converter
	public FaultList toErrore(Exception value, Exchange exchange) {
		ArrayList<Errore> aList = new ArrayList<Errore>();

		Errore errore = new Errore();
		String route = exchange.getProperty(Exchange.FAILURE_ENDPOINT, String.class);
		String sistema = exchange.getProperty(BaseRouteBuilder.PROPERTY_SISTEMA, String.class);
		if (sistema == null) {
			sistema = SISTEMA.SOA.name();
		}
		String message=null;
		if (value.getMessage()!=null){
			message=value.getMessage();
			if (message.length()>601){
				message=value.getMessage().substring(0,600);				
			}
			message=message.replaceAll("[^\\x00-\\x7F]", "");
		}
		if (value instanceof IllegalStateException) {
			errore.setCodice(Errore.CODICE_ERRORE_GENERICO);
			errore.setDescrizione("Queue Full " + route + " eccezione: " + message);
			errore.setSistema(sistema);
		} else if (value instanceof RejectedExecutionException) {
			errore.setCodice(Errore.CODICE_ERRORE_GENERICO);
			errore.setDescrizione("StuckConnections " + route + " eccezione: " + message);
			errore.setSistema(sistema);
		} else if (value instanceof ResourceNotFoundException) {
			errore.setCodice(Errore.CODICE_DATI_NON_TROVATI);
			errore.setDescrizione("Dati Non Trovati");
			errore.setSistema(((ResourceNotFoundException) value).getSistema());
		} else if ((value instanceof ExchangeTimedOutException) || (value instanceof SocketTimeoutException) ){
			errore.setCodice(Errore.CODICE_TIMEOUT);
			errore.setDescrizione("Timeout");
			errore.setSistema(sistema);
		}else if (value instanceof PredicateValidationException) {
			errore.setCodice(Errore.CODICE_PREDICATE_EXCPETION);
			errore.setDescrizione("Validation Exception on "+((PredicateValidationException)value).getPredicate());
			errore.setSistema(SISTEMA.SOA.name());
		} else {
			errore.setCodice(Errore.CODICE_ERRORE_GENERICO);
			errore.setDescrizione(route + " eccezione: " + message);
			errore.setSistema(sistema);
		}
		aList.add(errore);
		return new FaultList(aList);
	}
}

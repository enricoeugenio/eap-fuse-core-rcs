package it.rcs.fuse.core.cxf.configuration;

import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.configuration.security.AuthorizationPolicy;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.frontend.AbstractWSDLBasedEndpointFactory;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;

public class PolicyTimeoutAndBasicAuthenticationEnpointConfigurer
		implements org.apache.camel.component.cxf.CxfEndpointConfigurer {

	private AuthorizationPolicy autorizationPolicy = null;
	private int receiveTimeout;
	private boolean streamingEnable;
	private long connectionTimeout;

	public PolicyTimeoutAndBasicAuthenticationEnpointConfigurer(boolean streamingEnable) {
		this(-1, null, null, streamingEnable);
	}

	public PolicyTimeoutAndBasicAuthenticationEnpointConfigurer() {
		this(-1, null, null, true,10000);
	}

	public PolicyTimeoutAndBasicAuthenticationEnpointConfigurer(String username, String password,
			boolean streamingEnable) {
		this(-1, username, password, streamingEnable,10000);
	}

	public PolicyTimeoutAndBasicAuthenticationEnpointConfigurer(String username, String password) {
		this(-1, username, password, true,10000);
	}

	public PolicyTimeoutAndBasicAuthenticationEnpointConfigurer(int receiveTimeout) {
		this(receiveTimeout, null, null, true,10000);
	}

	public PolicyTimeoutAndBasicAuthenticationEnpointConfigurer(int receiveTimeout, boolean streamingEnable) {
		this(-1, null, null, streamingEnable,10000);
	}

	public PolicyTimeoutAndBasicAuthenticationEnpointConfigurer(int receiveTimeout, String username, String password) {
		this(receiveTimeout, username, password, true,10000);
	}
	public PolicyTimeoutAndBasicAuthenticationEnpointConfigurer(int receiveTimeout, String username, String password,
			boolean streamingEnable) {
		this(receiveTimeout, username, password, true,10000);
	}
	public PolicyTimeoutAndBasicAuthenticationEnpointConfigurer(int receiveTimeout, String username, String password,
			boolean streamingEnable,int connectionTimeout) {
		if (receiveTimeout > -1) {
			this.receiveTimeout = receiveTimeout;
		}
		this.connectionTimeout = connectionTimeout;
		if (username != null) {
			autorizationPolicy = new AuthorizationPolicy();
			autorizationPolicy.setUserName(username);
			autorizationPolicy.setPassword(password);
			autorizationPolicy.setAuthorizationType("Basic");
		}
		this.streamingEnable = streamingEnable;
	}

	public void configure(AbstractWSDLBasedEndpointFactory arg0) {

	}

	public void configureClient(Client client) {
		HTTPConduit conduit = (HTTPConduit) client.getConduit();
		HTTPClientPolicy policy = new HTTPClientPolicy();
		policy.setReceiveTimeout(receiveTimeout);
		policy.setConnectionTimeout(connectionTimeout);
		policy.setAllowChunking(streamingEnable);
		conduit.setClient(policy);
		TLSClientParameters tlsClientParameters = new TLSClientParameters();
		tlsClientParameters.setDisableCNCheck(true);
		conduit.setTlsClientParameters(tlsClientParameters);
		conduit.setAuthorization(autorizationPolicy);
	}

	public void configureServer(Server arg0) {

	}

}

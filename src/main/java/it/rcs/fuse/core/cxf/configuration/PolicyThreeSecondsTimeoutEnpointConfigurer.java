package it.rcs.fuse.core.cxf.configuration;

public class PolicyThreeSecondsTimeoutEnpointConfigurer extends PolicyTimeoutAndBasicAuthenticationEnpointConfigurer{

	public PolicyThreeSecondsTimeoutEnpointConfigurer(){
		super(3000);
	}
	public PolicyThreeSecondsTimeoutEnpointConfigurer(String username,String password){
		super(3000,username,password);
	}
	public PolicyThreeSecondsTimeoutEnpointConfigurer(String username, String password, boolean streaming) {
		super(3000, username, password, streaming);
	}

	public PolicyThreeSecondsTimeoutEnpointConfigurer(boolean streaming) {
		super(3000, streaming);
	}

}

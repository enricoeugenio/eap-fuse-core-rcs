package it.rcs.fuse.core.cxf.configuration;

public class PolicyFiveSecondsTimeoutEnpointConfigurer extends PolicyTimeoutAndBasicAuthenticationEnpointConfigurer{

	public PolicyFiveSecondsTimeoutEnpointConfigurer() {
		super(5000);
	}
	public PolicyFiveSecondsTimeoutEnpointConfigurer(String username,String password) {
		super(5000,username,password);
	}
	public PolicyFiveSecondsTimeoutEnpointConfigurer(String username, String password, boolean streaming) {
		super(5000, username, password, streaming);
	}

	public PolicyFiveSecondsTimeoutEnpointConfigurer(boolean streaming) {
		super(5000, streaming);
	}

}

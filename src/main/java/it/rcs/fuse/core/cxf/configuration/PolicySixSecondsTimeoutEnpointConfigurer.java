package it.rcs.fuse.core.cxf.configuration;

public class PolicySixSecondsTimeoutEnpointConfigurer extends PolicyTimeoutAndBasicAuthenticationEnpointConfigurer{

	public PolicySixSecondsTimeoutEnpointConfigurer() {
		super(6000);
	}
	public PolicySixSecondsTimeoutEnpointConfigurer(String username,String password) {
		super(6000,username,password);
	}
	public PolicySixSecondsTimeoutEnpointConfigurer(String username, String password, boolean streaming) {
		super(6000, username, password, streaming);
	}

	public PolicySixSecondsTimeoutEnpointConfigurer(boolean streaming) {
		super(6000, streaming);
	}

}

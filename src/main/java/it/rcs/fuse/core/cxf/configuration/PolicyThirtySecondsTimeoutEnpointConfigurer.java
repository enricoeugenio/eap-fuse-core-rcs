package it.rcs.fuse.core.cxf.configuration;

public class PolicyThirtySecondsTimeoutEnpointConfigurer extends PolicyTimeoutAndBasicAuthenticationEnpointConfigurer {

	public PolicyThirtySecondsTimeoutEnpointConfigurer() {
		super(30000);
	}

	public PolicyThirtySecondsTimeoutEnpointConfigurer(String username, String password) {
		super(30000, username, password);
	}

	public PolicyThirtySecondsTimeoutEnpointConfigurer(String username, String password, boolean streaming) {
		super(30000, username, password, streaming);
	}

	public PolicyThirtySecondsTimeoutEnpointConfigurer(boolean streaming) {
		super(30000, streaming);
	}
}

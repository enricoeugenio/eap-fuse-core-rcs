package it.rcs.fuse.core.cxf.configuration;

public class PolicyNinetySecondsTimeoutEnpointConfigurer extends PolicyTimeoutAndBasicAuthenticationEnpointConfigurer{

	public PolicyNinetySecondsTimeoutEnpointConfigurer() {
		super(90000);
	}
	public PolicyNinetySecondsTimeoutEnpointConfigurer(String username,String password) {
		super(90000,username,password);
	}
	public PolicyNinetySecondsTimeoutEnpointConfigurer(String username, String password, boolean streaming) {
		super(90000, username, password, streaming);
	}

	public PolicyNinetySecondsTimeoutEnpointConfigurer(boolean streaming) {
		super(90000, streaming);
	}

}

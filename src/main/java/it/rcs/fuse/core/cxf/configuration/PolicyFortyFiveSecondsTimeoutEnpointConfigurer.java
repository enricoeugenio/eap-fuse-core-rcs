package it.rcs.fuse.core.cxf.configuration;

public class PolicyFortyFiveSecondsTimeoutEnpointConfigurer extends PolicyTimeoutAndBasicAuthenticationEnpointConfigurer{

	public PolicyFortyFiveSecondsTimeoutEnpointConfigurer() {
		super(45000);
	}
	public PolicyFortyFiveSecondsTimeoutEnpointConfigurer(String username,String password) {
		super(45000,username,password);
	}
	public PolicyFortyFiveSecondsTimeoutEnpointConfigurer(String username, String password, boolean streaming) {
		super(45000, username, password, streaming);
	}

	public PolicyFortyFiveSecondsTimeoutEnpointConfigurer(boolean streaming) {
		super(45000, streaming);
	}

}

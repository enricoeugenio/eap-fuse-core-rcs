package it.rcs.fuse.core.cxf.configuration;

public class PolicyOneHundredFiftySecondsTimeoutEnpointConfigurer extends PolicyTimeoutAndBasicAuthenticationEnpointConfigurer{

	public PolicyOneHundredFiftySecondsTimeoutEnpointConfigurer() {
		super(150000);
	}
	public PolicyOneHundredFiftySecondsTimeoutEnpointConfigurer(String username,String password) {
		super(150000,username,password);
	}
	public PolicyOneHundredFiftySecondsTimeoutEnpointConfigurer(String username, String password, boolean streaming) {
		super(150000, username, password, streaming);
	}

	public PolicyOneHundredFiftySecondsTimeoutEnpointConfigurer(boolean streaming) {
		super(150000, streaming);
	}

}

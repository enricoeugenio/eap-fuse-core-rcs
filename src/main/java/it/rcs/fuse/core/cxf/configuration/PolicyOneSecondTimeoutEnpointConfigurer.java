package it.rcs.fuse.core.cxf.configuration;

public class PolicyOneSecondTimeoutEnpointConfigurer extends PolicyTimeoutAndBasicAuthenticationEnpointConfigurer{

	public PolicyOneSecondTimeoutEnpointConfigurer() {
		super(1000);
	}
	public PolicyOneSecondTimeoutEnpointConfigurer(String username,String password) {
		super(1000,username,password);
	}
	public PolicyOneSecondTimeoutEnpointConfigurer(String username, String password, boolean streaming) {
		super(1000, username, password, streaming);
	}

	public PolicyOneSecondTimeoutEnpointConfigurer(boolean streaming) {
		super(1000, streaming);
	}

}

package it.rcs.fuse.core.cxf.configuration;

public class PolicyTenSecondsTimeoutEnpointConfigurer extends PolicyTimeoutAndBasicAuthenticationEnpointConfigurer{

	public PolicyTenSecondsTimeoutEnpointConfigurer(){
		super(10000);
	}
	public PolicyTenSecondsTimeoutEnpointConfigurer(String username,String password){
		super(10000,username,password);
	}
	
	public PolicyTenSecondsTimeoutEnpointConfigurer(String username, String password, boolean streaming) {
		super(10000, username, password, streaming);
	}

	public PolicyTenSecondsTimeoutEnpointConfigurer(boolean streaming) {
		super(10000, streaming);
	}


}

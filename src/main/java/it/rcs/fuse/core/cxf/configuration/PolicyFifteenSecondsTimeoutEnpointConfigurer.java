package it.rcs.fuse.core.cxf.configuration;

public class PolicyFifteenSecondsTimeoutEnpointConfigurer extends PolicyTimeoutAndBasicAuthenticationEnpointConfigurer {

	public PolicyFifteenSecondsTimeoutEnpointConfigurer() {
		super(15000);
	}

	public PolicyFifteenSecondsTimeoutEnpointConfigurer(String username, String password) {
		super(15000, username, password);
	}

	public PolicyFifteenSecondsTimeoutEnpointConfigurer(String username, String password, boolean streaming) {
		super(15000, username, password, streaming);
	}

	public PolicyFifteenSecondsTimeoutEnpointConfigurer(boolean streaming) {
		super(15000, streaming);
	}
}
